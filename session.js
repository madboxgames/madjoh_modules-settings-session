define([
	'require',
	'madjoh_modules/custom_events/custom_events'
],
function(require, CustomEvents){
	var Session = {
		onLoggedOut : function(){
			CustomEvents.fireCustomEvent(document, 'ChangePage', {key : 'AccountLogin'});
		}
	};

	return Session;
});